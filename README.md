LV6 Analysis 1
Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost
znanstvenog kalkulatora, odnosno implementirati osnovne (+,-,*,/) i barem 5
naprednih (sin, cos, log, sqrt...) operacija

LV6 Analysis 2
Napravite jednostavnu igru vješala. Pojmovi se učitavaju u listu iz datoteke, i u
svakoj partiji se odabire nasumični pojam iz liste. Omogućiti svu
funkcionalnost koju biste očekivali od takve igre. Nije nužno crtati vješala,
dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo. 