﻿namespace LV6_Analysis_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bPlus = new System.Windows.Forms.Button();
            this.bMulti = new System.Windows.Forms.Button();
            this.bDivide = new System.Windows.Forms.Button();
            this.bMinus = new System.Windows.Forms.Button();
            this.tbOperator1 = new System.Windows.Forms.TextBox();
            this.lblOperation = new System.Windows.Forms.Label();
            this.tbOperator2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbResult = new System.Windows.Forms.TextBox();
            this.bExit = new System.Windows.Forms.Button();
            this.tbOperator = new System.Windows.Forms.TextBox();
            this.tbCResult = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.bSine = new System.Windows.Forms.Button();
            this.bCosine = new System.Windows.Forms.Button();
            this.bRoot = new System.Windows.Forms.Button();
            this.bSquare = new System.Windows.Forms.Button();
            this.bLog = new System.Windows.Forms.Button();
            this.bLn = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.bCClear = new System.Windows.Forms.Button();
            this.bAllClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bPlus
            // 
            this.bPlus.Location = new System.Drawing.Point(12, 38);
            this.bPlus.Name = "bPlus";
            this.bPlus.Size = new System.Drawing.Size(23, 23);
            this.bPlus.TabIndex = 0;
            this.bPlus.Text = "+";
            this.bPlus.UseVisualStyleBackColor = true;
            this.bPlus.Click += new System.EventHandler(this.bPlus_Click);
            // 
            // bMulti
            // 
            this.bMulti.Location = new System.Drawing.Point(12, 67);
            this.bMulti.Name = "bMulti";
            this.bMulti.Size = new System.Drawing.Size(23, 23);
            this.bMulti.TabIndex = 1;
            this.bMulti.Text = "*";
            this.bMulti.UseVisualStyleBackColor = true;
            this.bMulti.Click += new System.EventHandler(this.bMulti_Click);
            // 
            // bDivide
            // 
            this.bDivide.Location = new System.Drawing.Point(41, 67);
            this.bDivide.Name = "bDivide";
            this.bDivide.Size = new System.Drawing.Size(23, 23);
            this.bDivide.TabIndex = 2;
            this.bDivide.Text = "/";
            this.bDivide.UseVisualStyleBackColor = true;
            this.bDivide.Click += new System.EventHandler(this.bDivide_Click);
            // 
            // bMinus
            // 
            this.bMinus.Location = new System.Drawing.Point(41, 38);
            this.bMinus.Name = "bMinus";
            this.bMinus.Size = new System.Drawing.Size(23, 23);
            this.bMinus.TabIndex = 3;
            this.bMinus.Text = "-";
            this.bMinus.UseVisualStyleBackColor = true;
            this.bMinus.Click += new System.EventHandler(this.bMinus_Click);
            // 
            // tbOperator1
            // 
            this.tbOperator1.Location = new System.Drawing.Point(12, 12);
            this.tbOperator1.Name = "tbOperator1";
            this.tbOperator1.Size = new System.Drawing.Size(72, 20);
            this.tbOperator1.TabIndex = 4;
            this.tbOperator1.TextChanged += new System.EventHandler(this.tbOperator1_TextChanged);
            // 
            // lblOperation
            // 
            this.lblOperation.AutoSize = true;
            this.lblOperation.Location = new System.Drawing.Point(90, 15);
            this.lblOperation.Name = "lblOperation";
            this.lblOperation.Size = new System.Drawing.Size(0, 13);
            this.lblOperation.TabIndex = 5;
            // 
            // tbOperator2
            // 
            this.tbOperator2.Location = new System.Drawing.Point(109, 12);
            this.tbOperator2.Name = "tbOperator2";
            this.tbOperator2.Size = new System.Drawing.Size(72, 20);
            this.tbOperator2.TabIndex = 6;
            this.tbOperator2.TextChanged += new System.EventHandler(this.tbOperator2_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(187, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "=";
            // 
            // tbResult
            // 
            this.tbResult.Location = new System.Drawing.Point(206, 12);
            this.tbResult.Name = "tbResult";
            this.tbResult.ReadOnly = true;
            this.tbResult.Size = new System.Drawing.Size(117, 20);
            this.tbResult.TabIndex = 8;
            // 
            // bExit
            // 
            this.bExit.Location = new System.Drawing.Point(248, 258);
            this.bExit.Name = "bExit";
            this.bExit.Size = new System.Drawing.Size(75, 23);
            this.bExit.TabIndex = 9;
            this.bExit.Text = "Exit";
            this.bExit.UseVisualStyleBackColor = true;
            this.bExit.Click += new System.EventHandler(this.bExit_Click);
            // 
            // tbOperator
            // 
            this.tbOperator.Location = new System.Drawing.Point(52, 145);
            this.tbOperator.Name = "tbOperator";
            this.tbOperator.Size = new System.Drawing.Size(100, 20);
            this.tbOperator.TabIndex = 10;
            this.tbOperator.TextChanged += new System.EventHandler(this.tbOperator_TextChanged);
            // 
            // tbCResult
            // 
            this.tbCResult.Location = new System.Drawing.Point(177, 145);
            this.tbCResult.Name = "tbCResult";
            this.tbCResult.ReadOnly = true;
            this.tbCResult.Size = new System.Drawing.Size(100, 20);
            this.tbCResult.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(158, 148);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "=";
            // 
            // bSine
            // 
            this.bSine.Location = new System.Drawing.Point(43, 172);
            this.bSine.Name = "bSine";
            this.bSine.Size = new System.Drawing.Size(75, 23);
            this.bSine.TabIndex = 13;
            this.bSine.Text = "sin";
            this.bSine.UseVisualStyleBackColor = true;
            this.bSine.Click += new System.EventHandler(this.bSine_Click);
            // 
            // bCosine
            // 
            this.bCosine.Location = new System.Drawing.Point(43, 201);
            this.bCosine.Name = "bCosine";
            this.bCosine.Size = new System.Drawing.Size(75, 23);
            this.bCosine.TabIndex = 14;
            this.bCosine.Text = "cos";
            this.bCosine.UseVisualStyleBackColor = true;
            this.bCosine.Click += new System.EventHandler(this.bCosine_Click);
            // 
            // bRoot
            // 
            this.bRoot.Location = new System.Drawing.Point(125, 171);
            this.bRoot.Name = "bRoot";
            this.bRoot.Size = new System.Drawing.Size(75, 23);
            this.bRoot.TabIndex = 15;
            this.bRoot.Text = "sqrt";
            this.bRoot.UseVisualStyleBackColor = true;
            this.bRoot.Click += new System.EventHandler(this.bRoot_Click);
            // 
            // bSquare
            // 
            this.bSquare.Location = new System.Drawing.Point(125, 201);
            this.bSquare.Name = "bSquare";
            this.bSquare.Size = new System.Drawing.Size(75, 23);
            this.bSquare.TabIndex = 16;
            this.bSquare.Text = "^2";
            this.bSquare.UseVisualStyleBackColor = true;
            this.bSquare.Click += new System.EventHandler(this.bSquare_Click);
            // 
            // bLog
            // 
            this.bLog.Location = new System.Drawing.Point(207, 171);
            this.bLog.Name = "bLog";
            this.bLog.Size = new System.Drawing.Size(75, 23);
            this.bLog.TabIndex = 17;
            this.bLog.Text = "log10";
            this.bLog.UseVisualStyleBackColor = true;
            this.bLog.Click += new System.EventHandler(this.bLog_Click);
            // 
            // bLn
            // 
            this.bLn.Location = new System.Drawing.Point(207, 201);
            this.bLn.Name = "bLn";
            this.bLn.Size = new System.Drawing.Size(75, 23);
            this.bLn.TabIndex = 18;
            this.bLn.Text = "ln";
            this.bLn.UseVisualStyleBackColor = true;
            this.bLn.Click += new System.EventHandler(this.bLn_Click);
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(80, 51);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(23, 23);
            this.bClear.TabIndex = 19;
            this.bClear.Text = "C";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // bCClear
            // 
            this.bCClear.Location = new System.Drawing.Point(288, 186);
            this.bCClear.Name = "bCClear";
            this.bCClear.Size = new System.Drawing.Size(23, 23);
            this.bCClear.TabIndex = 20;
            this.bCClear.Text = "C";
            this.bCClear.UseVisualStyleBackColor = true;
            this.bCClear.Click += new System.EventHandler(this.bCClear_Click);
            // 
            // bAllClear
            // 
            this.bAllClear.Location = new System.Drawing.Point(12, 258);
            this.bAllClear.Name = "bAllClear";
            this.bAllClear.Size = new System.Drawing.Size(75, 23);
            this.bAllClear.TabIndex = 21;
            this.bAllClear.Text = "Clear All";
            this.bAllClear.UseVisualStyleBackColor = true;
            this.bAllClear.Click += new System.EventHandler(this.bAllClear_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 293);
            this.Controls.Add(this.bAllClear);
            this.Controls.Add(this.bCClear);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bLn);
            this.Controls.Add(this.bLog);
            this.Controls.Add(this.bSquare);
            this.Controls.Add(this.bRoot);
            this.Controls.Add(this.bCosine);
            this.Controls.Add(this.bSine);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbCResult);
            this.Controls.Add(this.tbOperator);
            this.Controls.Add(this.bExit);
            this.Controls.Add(this.tbResult);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbOperator2);
            this.Controls.Add(this.lblOperation);
            this.Controls.Add(this.tbOperator1);
            this.Controls.Add(this.bMinus);
            this.Controls.Add(this.bDivide);
            this.Controls.Add(this.bMulti);
            this.Controls.Add(this.bPlus);
            this.Name = "Form1";
            this.Text = "Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bPlus;
        private System.Windows.Forms.Button bMulti;
        private System.Windows.Forms.Button bDivide;
        private System.Windows.Forms.Button bMinus;
        private System.Windows.Forms.TextBox tbOperator1;
        private System.Windows.Forms.Label lblOperation;
        private System.Windows.Forms.TextBox tbOperator2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbResult;
        private System.Windows.Forms.Button bExit;
        private System.Windows.Forms.TextBox tbOperator;
        private System.Windows.Forms.TextBox tbCResult;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bSine;
        private System.Windows.Forms.Button bCosine;
        private System.Windows.Forms.Button bRoot;
        private System.Windows.Forms.Button bSquare;
        private System.Windows.Forms.Button bLog;
        private System.Windows.Forms.Button bLn;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Button bCClear;
        private System.Windows.Forms.Button bAllClear;
    }
}

